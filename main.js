const canvas = document.querySelector('canvas');
const scoreElement = document.querySelector('#score');

const ctx = canvas.getContext('2d');

canvas.width = innerWidth;
canvas.height = innerHeight;

class Boundary {
    static width = 40;
    static height = 40;
    constructor({ position, image}){
        this.position = position;
        this.width = 40;
        this.height = 40;
        this.image = image;
    }

    draw() {
        // ctx.fillStyle = 'blue';
        // ctx.fillRect(this.position.x, this.position.y, this.width, this.height)
        ctx.drawImage(this.image, this.position.x, this.position.y);
    }
}

class Pellet {
    constructor({ position }) {
        this.position = position;
        this.radius = 3;
    }

    draw() {
        ctx.beginPath();
        ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.closePath();
    }
}

class PowerUp {
    constructor({ position }) {
        this.position = position;
        this.radius = 8;
    }

    draw() {
        ctx.beginPath();
        ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
        ctx.fillStyle = 'white';
        ctx.fill();
        ctx.closePath();
    }
}

class Player {
    constructor({position, velocity}) {
        this.position = position;
        this.velocity = velocity;
        this.radius = 15;
        this.radians = 0.75;
        this.openRate = 0.12;
        this.rotation = 0;
    }

    draw() {
        ctx.save();
        ctx.translate(this.position.x, this.position.y);
        ctx.rotate(this.rotation);
        ctx.translate(-this.position.x, -this.position.y);
        ctx.beginPath();
        ctx.arc(
            this.position.x, 
            this.position.y, 
            this.radius, 
            this.radians, // angle  
            Math.PI * 2 - this.radians
        );
        ctx.lineTo(this.position.x, this.position.y);
        ctx.fillStyle = 'yellow';
        ctx.fill();
        ctx.closePath();
        ctx.restore();
    }

    update() {
        this.draw();
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
        if (this.radians < 0 || this.radians > 0.75) this.openRate = -this.openRate;
        this.radians += this.openRate;
    }
}

class Ghost {
    static speed = 2;
    constructor({ position, velocity, color = 'red'}) {
        this.position = position;
        this.velocity = velocity;
        this.radius = 15;
        this.color = color;
        this.prevCollisions = [];
        this.speed = 2;
        this.scared = false;
    }

    draw() {
        ctx.beginPath();
        ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
        ctx.fillStyle = this.scared ? 'blue' : this.color;
        ctx.fill();
        ctx.closePath();
    }

    update() {
        this.draw();
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    }
}

const pellets = [];

const boundaries = [];

const powerUps = [];

const ghosts = [
    new Ghost({
        position: {
            x: Boundary.width * 6 + Boundary.width / 2,
            y: Boundary.height + Boundary.height / 2
        },
        velocity: {
            y: 0,
            x: Ghost.speed
        }
    }),
    new Ghost({
        position: {
            x: Boundary.width * 6 + Boundary.width / 2,
            y: Boundary.height * 3 + Boundary.height / 2
        },
        velocity: {
            y: 0,
            x: Ghost.speed
        },
        color: 'pink'
    })
];

const player = new Player({
    position: {
        x: Boundary.width + Boundary.width / 2,
        y: Boundary.height + Boundary.height / 2
    },
    velocity: {
        x: 0,
        y: 0
    }
})

// to determed what keys are pressed
const keys = {
   u: {
    pressed: false
   },
   h: {
    pressed: false
   },
   j: {
    pressed: false
   },
   k: {
    pressed: false
   }
}

let lastKey = '';

let score = 0;

/* const map = [
    ['-', '-', '-', '-', '-', '-', '-'], 
    ['-', ' ', ' ', ' ', ' ', ' ', '-'], 
    ['-', ' ', '-', ' ', '-', ' ', '-'], 
    ['-', ' ', ' ', ' ', ' ', ' ', '-'], 
    ['-', ' ', '-', ' ', '-', ' ', '-'], 
    ['-', ' ', ' ', ' ', ' ', ' ', '-'], 
    ['-', '-', '-', '-', '-', '-', '-'] 
]; 
const firstMapWithImages = [
    ['1', '-', '-', '-', '-', '-', '2'], 
    ['|', ' ', ' ', ' ', ' ', ' ', '|'], 
    ['|', ' ', 'b', ' ', 'b', ' ', '|'], 
    ['|', ' ', ' ', ' ', ' ', ' ', '|'], 
    ['|', ' ', 'b', ' ', 'b', ' ', '|'], 
    ['|', ' ', ' ', ' ', ' ', ' ', '|'], 
    ['4', '-', '-', '-', '-', '-', '3'] 
];*/

/**
 * create a symetrical map, using pipes, blocks & caps images
 */
const map = [
  ['1', '-', '-', '-', '-', '-', '-', '-', '-', '-', '2'],
  ['|', '.', '.', '.', '.', '.', '.', '.', '.', '.', '|'],
  ['|', '.', 'b', '.', '[', '7', ']', '.', 'b', '.', '|'],
  ['|', '.', '.', '.', '.', '_', '.', '.', '.', '.', '|'],
  ['|', '.', '[', ']', '.', '.', '.', '[', ']', '.', '|'],
  ['|', '.', '.', '.', '.', '^', '.', '.', '.', '.', '|'],
  ['|', '.', 'b', '.', '[', '+', ']', '.', 'b', '.', '|'],
  ['|', '.', '.', '.', '.', '_', '.', '.', '.', '.', '|'],
  ['|', '.', '[', ']', '.', '.', '.', '[', ']', '.', '|'],
  ['|', '.', '.', '.', '.', '^', '.', '.', '.', '.', '|'],
  ['|', '.', 'b', '.', '[', '5', ']', '.', 'b', '.', '|'],
  ['|', '.', '.', '.', '.', '.', '.', '.', '.', 'p', '|'],
  ['4', '-', '-', '-', '-', '-', '-', '-', '-', '-', '3']
]

/* const map2 = [
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
  ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'],
] */

function createImage(src){
    const image = new Image();   
    image.src = src;
    return image;
}

map.forEach((row, i) => {
    row.forEach((symbol, j) => {
        //console.log(symbol);
        switch (symbol) {
            case '1':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i or index is equal to columns/row number
                        },
                        image: createImage('./img/pipeCorner1.png')
                    }) 
                )
                break;
            case '2':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i or index is equal to columns/row number
                        },
                        image: createImage('./img/pipeCorner2.png')
                    }) 
                )
                break;
            case '3':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i or index is equal to columns/row number
                        },
                        image: createImage('./img/pipeCorner3.png')
                    }) 
                )
                break;
            case '4':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i or index is equal to columns/row number
                        },
                        image: createImage('./img/pipeCorner4.png')
                    }) 
                )
                break;
            case '-':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i or index is equal to columns/row number
                        },
                        image: createImage('./img/pipeHorizontal.png')
                    }) 
                )
                break;
            case '.':
                pellets.push(
                    new Pellet({
                        position: {
                            x: j * Boundary.width + Boundary.width / 2,
                            y: i * Boundary.height + Boundary.height / 2
                        }
                    })
                )
                break;
            case 'p':
                powerUps.push(
                    new PowerUp({
                        position: {
                            x: j * Boundary.width + Boundary.width / 2,
                            y: i * Boundary.height + Boundary.height / 2
                        }
                    })
                )
                break;

            case 'b':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i or index is equal to columns/row number
                        },
                        image: createImage('./img/block.png')
                    }) 
                )
                break;
            case '|':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: Boundary.width * j,
                            y: Boundary.height * i // i (as index) equal to rows number
                        },
                        image: createImage('./img/pipeVertical.png')
                    }) 
                )
                break;
            case '[':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        image: createImage('./img/capLeft.png')
                    })
                )
                break;
            case ']':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        image: createImage('./img/capRight.png')
                    })
                )
                break;
            case '_':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        image: createImage('./img/capBottom.png')
                    })
                )
                break;
            case '^':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        image: createImage('./img/capTop.png')
                    })
                )
                break;
            case '+':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        image: createImage('./img/pipeCross.png')
                    })
                )
                break;
            case '5':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        color: 'blue',
                        image: createImage('./img/pipeConnectorTop.png')
                    })
                )
                break;
            case '6':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        color: 'blue',
                        image: createImage('./img/pipeConnectorRight.png')
                    })
                )
                break;
            case '7':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        color: 'blue',
                        image: createImage('./img/pipeConnectorBottom.png')
                    })
                )
                break;
            case '8':
                boundaries.push(
                    new Boundary({
                        position: {
                            x: j * Boundary.width,
                            y: i * Boundary.height
                        },
                        image: createImage('./img/pipeConnectorLeft.png')
                    })
                )
        }
    })
})                  

function circleCollideWithRectangle({ circle, rectangle }) {
    const padding = Boundary.width / 2 - circle. radius -1;
    return (
        circle.position.y - circle.radius + circle.velocity.y 
        <= rectangle.position.y + rectangle.height + padding
        && circle.position.x + circle.radius 
        + circle.velocity.x >= rectangle.position.x - padding  
        && circle.position.y + circle.radius 
        + circle.velocity.y >= rectangle.position.y - padding
        && circle.position.x - circle.radius + circle.velocity.x 
        <= rectangle.position.x + rectangle.width + padding
    )
}

let animationId;
function animate() {
    animationId = requestAnimationFrame(animate);
    ctx.clearRect(0, 0, canvas.width, canvas.height);   
    
    if (keys.u.pressed && lastKey === 'u') {
        for (let i = 0; i < boundaries.length; i++) {
            const boundary = boundaries[i];   
            if (
                circleCollideWithRectangle({
                    circle: {
                        ...player, 
                        velocity: {
                            x: 0,
                            y: -5
                        }
                    }, 
                    rectangle: boundary
                })
            ) {
                player.velocity.y = 0; 
                break;           
            } else {
                player.velocity.y = -5;
            }
        }
    } else if (keys.h.pressed && lastKey === 'h') {
        for (let i = 0; i < boundaries.length; i++) {
            const boundary = boundaries[i];   
            if (
                circleCollideWithRectangle({
                    circle: {
                        ...player, 
                        velocity: {
                            x: -5,
                            y: 0
                        }
                    }, 
                    rectangle: boundary
                })
            ) {
                player.velocity.x = 0; 
                break;           
            } else {
                player.velocity.x = -5;
            }
        }
    } else if (keys.j.pressed && lastKey === 'j') {
        for (let i = 0; i < boundaries.length; i++){
            const boundary = boundaries[i];   
            if (circleCollideWithRectangle({
                circle: {
                    ...player, 
                    velocity: {
                        x: 0,
                        y: 5
                    }
                }, 
                rectangle: boundary
            })
        ) {
            player.velocity.y = 0; 
            break;           
        } else {
            player.velocity.y = 5;
        }
        }
    } else if (keys.k.pressed && lastKey === 'k') {
        for (let i = 0; i < boundaries.length; i++){
            const boundary = boundaries[i];   
            if (
                circleCollideWithRectangle({
                    circle: {
                        ...player, 
                        velocity: {
                            x: 5,
                            y: 0
                        }
                    }, 
                rectangle: boundary
                })
            ) {
                player.velocity.x = 0; 
                break;           
            } else {
                player.velocity.x = 5;
            }
        }
    }

    // detect collision between player & ghosts
    for (let i = ghosts.length - 1; 0 <= i; i--){
        const ghost = ghosts[i];
        // ghost & player collision condition 
        if (Math.hypot(
                ghost.position.x - player.position.x, 
                ghost.position.y - player.position.y
            ) < ghost.radius + player.radius) {
                    if (ghost.scared) {
                        ghosts.splice(i, 1);
                    } else {
                        cancelAnimationFrame(animationId);
                        alert('Game Over');
                        console.log('game over');
                    }
            }
        }

    // win condition
    if (pellets.length === 0){
        alert('Bravo, vous avez gagné!');
        console.log('gagné');
        cancelAnimationFrame(animationId);
        
    }
    // power ups iteration
    for (let i = powerUps.length - 1; 0 <= i; i--){
        const powerUp = powerUps[i];
        powerUp.draw();

        if (Math.hypot(
                powerUp.position.x - player.position.x, 
                powerUp.position.y - player.position.y
            ) < powerUp.radius + player.radius) {
                    console.log('chat...touché...');
                    powerUps.splice(i, 1);
                    score += 100;
                    scoreElement.innerHTML = score;

                    // create ghost scared
                    ghosts.forEach(ghost => {
                       ghost.scared = true;
                       console.log(ghost.scared);
                       setTimeout(() => {
                          ghost.scared = false;
                          console.log(ghost.scared);
                       }, 7000) 
                    })
        }
    }

    // touch pellets on following `for` loop reverse iteration
    for (let i = pellets.length - 1; 0 <= i; i--){
        const pellet = pellets[i];
    /*  Replace `forEach` by `for` loop to iterate on reverse, less 1 by less 1 
        using "i--", so, comment following `forEach` fnc statement: ``` } pellets.forEach((pellet, i) => { ``` */
        pellet.draw();

        if (Math.hypot(
                pellet.position.x - player.position.x, 
                pellet.position.y - player.position.y
            ) < pellet.radius + player.radius) {
                    console.log('chat...touché...');
                    pellets.splice(i, 1);
                    score += 10;
                    scoreElement.innerHTML = score;
        }
    }
    /**
     * Collision detection code
     */
    boundaries.forEach((boundary) => {
        boundary.draw();

        if (circleCollideWithRectangle({
            circle: player,
            rectangle: boundary
        })) 
        {
                console.log('boum! poum! paf! collision!');
                player.velocity.y = 0;
                player.velocity.x = 0;
        }
    })
    player.update();

    ghosts.forEach((ghost) => {
        // console.log('l\'fantôme arrive! aaahhh!');
        ghost.update();

        // ghost & player collision condition 
        if (Math.hypot(
                ghost.position.x - player.position.x, 
                ghost.position.y - player.position.y
            ) < ghost.radius + player.radius && !ghost.scared) {
                    cancelAnimationFrame(animationId);
                    alert('Game Over');
                    console.log('game over');
            }
        const collisions = []; // ghost collision array

        boundaries.forEach(boundary => {

            // RIGHT collision
            if (
                !collisions.includes('right') && 
                circleCollideWithRectangle({
                    circle: {
                        ...ghost, 
                        velocity: {
                            x: ghost.speed,
                            y: 0
                        }
                    },
                    rectangle: boundary
                })
            ) {
                collisions.push('right');
            }

            // LEFT collision
            if (
                !collisions.includes('left') && 
                circleCollideWithRectangle({
                    circle: {
                        ...ghost, 
                        velocity: {
                            x: -ghost.speed,
                            y: 0
                        }
                    },
                    rectangle: boundary
                })
            ) {
                collisions.push('left');
            }

            // TOP collision
            if (
                !collisions.includes('up') && 
                circleCollideWithRectangle({
                    circle: {
                        ...ghost, 
                        velocity: {
                            x: 0,
                            y: -ghost.speed
                        }
                    },
                    rectangle: boundary
                })
            ) {
                collisions.push('up');
            }

            // BOTTOM collision
            if (
                !collisions.includes('down') && 
                circleCollideWithRectangle({
                    circle: {
                        ...ghost, 
                        velocity: {
                            x: 0,
                            y: ghost.speed
                        }
                    },
                    rectangle: boundary
                })
            ) {
                collisions.push('down');
            }
        })

        if (collisions.length > ghost.prevCollisions.length) {
            ghost.prevCollisions = collisions;        
        }

        // replace array to string using `stringify` method
        if (JSON.stringify(collisions) !== JSON.stringify(ghost.prevCollisions)) {
            // console.log('pafff###!!!@@@&~@#!!@##@!poufff');
            
            if (ghost.velocity.x > 0) ghost.prevCollisions.push('right')
            else if (ghost.velocity.x < 0) ghost.prevCollisions.push('left')
            else if (ghost.velocity.y < 0) ghost.prevCollisions.push('up')
            else if (ghost.velocity.y > 0) ghost.prevCollisions.push('down')
            // console.log('collis: ' + collisions);
            // console.log('prev. collis: ' + ghost.prevCollisions);

            const pathways = ghost.prevCollisions.filter(collision => {
                return !collisions.includes(collision);
            })
            // console.log({ pathways });

            const directions = pathways[Math.floor(Math.random() * pathways.length)];
            // console.log(directions);

            switch (directions) {
                case 'down':
                    ghost.velocity.y = ghost.speed;
                    ghost.velocity.x = 0;
                    break;

                case 'up':
                    ghost.velocity.y = -ghost.speed;
                    ghost.velocity.x = 0;
                    break;

                case 'right':
                    ghost.velocity.y = 0;
                    ghost.velocity.x = ghost.speed;
                    break;

                case 'left':
                    ghost.velocity.y = 0;
                    ghost.velocity.x = -ghost.speed;
                    break;
            }
            ghost.prevCollisions = [];
        }
        
        // console.log('boom! fantôme collision...');
    })

    // reference player direction (for create pacman animation)
    if (player.velocity.x > 0) player.rotation = 0;
    else if (player.velocity.x < 0) player.rotation = Math.PI;
    else if (player.velocity.y > 0) player.rotation = Math.PI / 2;
    else if (player.velocity.y < 0) player.rotation = Math.PI * 1.5;
}

animate();

window.addEventListener('keydown', ({key}) => {
    // console.log(event.key);
    switch (key) {
        case 'u': 
            keys.u.pressed = true;
            lastKey = 'u';
            // player.velocity.y = -5;
            break; 
        case 'h':
            keys.h.pressed = true;
            lastKey = 'h';
            // player.velocity.x = -5;
            break;
        case 'j': 
            keys.j.pressed = true;
            lastKey = 'j';
            // player.velocity.y = 5;
            break; 
        case 'k':
            keys.k.pressed = true;
            lastKey = 'k';
            // player.velocity.x = 5;
    }
    
    // console.log(keys.k.pressed);
    // console.log(keys.j.pressed);
})

window.addEventListener('keyup', ({key}) => {
    
    switch (key) {
        case 'u': 
            keys.u.pressed = false;
            // player.velocity.y = 0;
            break; 
        case 'h':
            keys.h.pressed = false;
            // player.velocity.x = 0;
            break;
        case 'j': 
            keys.j.pressed = false;
            // player.velocity.y = 0;
            break; 
        case 'k':
            keys.k.pressed = false;
            // player.velocity.x = 0;
    }
    // console.log(keys.k.pressed);
    // console.log(keys.j.pressed);
})
